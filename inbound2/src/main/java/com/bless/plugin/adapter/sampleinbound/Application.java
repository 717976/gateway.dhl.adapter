package com.bless.plugin.adapter.sampleinbound;

import java.util.Optional;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;

import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.bless.crypto.constants.VaultType;
import com.bless.plugin.adapter.sampleinbound.callback.SNSInboundRequestReceiveCallback;
import com.bless.plugin.adapter.sdk.processor.security.PermitAllAuthenticator;
import com.bless.plugin.adapter.sdk.processor.security.RestRouteAuthenticator;
import com.bless.plugin.adapter.sdk.transformer.TransformerClient;


import lombok.extern.slf4j.Slf4j;
import okhttp3.logging.HttpLoggingInterceptor;

@SpringBootApplication
@ComponentScan(basePackages = {"com.bless.plugin.adapter.sdk"})
@EnableAutoConfiguration
@EnableConfigurationProperties
@PropertySource("application.yml")
@Slf4j
public class Application {
	@Value("${bless.transformer.url}")
	private String TRANSFORMER_URL;
	
	@Value("${bless.root.url}")
	private String BLESS_ROOT_URL;
	
	@Value("${bless.client.identity}")
	private String identity;
	
	@Value("${bless.vault.type}")
	private VaultType vaultType;
	
	@Value("${bless.vault.url}")
	private Optional<String> oVaultUrl;
	
	@Value("${bless.vault.token}")
	private String vaultToken;
	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CamelContextConfiguration contextConfiguration() {
        return new CamelContextConfiguration() {
          // @Override
            public void beforeApplicationStart(CamelContext context) {
                context.setUseMDCLogging(true);
            }

            //@Override
            public void afterApplicationStart(CamelContext camelContext) {
            }
        };
    }
    @Bean({"authenticator.inbound.secondary.rest", "authenticator.outbound.primary"})
    public RestRouteAuthenticator restRouteAuthenticator() {
        return new PermitAllAuthenticator();
    }
    @Bean
    public TransformerClient transformerClient() throws Exception {
    	HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        TransformerClient.init(TRANSFORMER_URL, interceptor);
        TransformerClient transformerClient = null;
        log.info("Transformer Client Bean Created");
		try {
			transformerClient = TransformerClient.getInstance();
		} catch (Exception e) {
			 throw new Exception("Transformer client not initialized : call init() method first");
		}
        return transformerClient;
    }
    @Bean({"callback.onreceive.custom"})
	public SNSInboundRequestReceiveCallback snsCallback() {
	return new SNSInboundRequestReceiveCallback();
}

	 //Created ConnectionFactory Bean for ActiveMQ
    @Bean
    public ActiveMQConnectionFactory connectionFactory() {
    	ActiveMQConnectionFactory f=new ActiveMQConnectionFactory();
    	f.setBrokerURL("tcp://localhost:61616");
    	f.setUserName("admin");
    	f.setPassword("admin");
    	return f;
    }
  
   /* @Bean
    public void sftpSessionFactory() {
    	SftpConfiguration sftpconfig=new SftpConfiguration();
    	sftpconfig.setBindAddress("sftp://blessdev@13.78.63.250");
    	sftpconfig.setDirectory("messageDir");
    	sftpconfig.setPrivateKeyFile("blessdev.pem");
    	 
    }*/


}
