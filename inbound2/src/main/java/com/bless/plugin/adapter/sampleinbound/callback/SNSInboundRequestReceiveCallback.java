package com.bless.plugin.adapter.sampleinbound.callback;

import java.io.IOException;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.Message;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;
import com.bless.plugin.client.exception.InvalidJsonPathException;
import com.bless.plugin.client.utility.JSONUtility;
import lombok.extern.slf4j.Slf4j;

@Component(SNSInboundRequestReceiveCallback.NAME)
@Slf4j
public class SNSInboundRequestReceiveCallback {
	public static final String NAME = "callback.onreceive.custom";
	private final String SNS_HEADER = "x-amz-sns-message-type";
	private final String SNS_SUBCONF_HEADER_VALUE = "SubscriptionConfirmation";
	private final String SNS_NOTIFICATN_HEADER_VALUE = "Notification";

	@Handler
	public void format(@Body Message message, Exchange exchange) throws Exception {
		Object header = message.getHeader(SNS_HEADER);
		log.debug("SNS Header: " + header.toString());
		if (header.toString().equalsIgnoreCase(SNS_SUBCONF_HEADER_VALUE)) {
			log.debug("Subscription Message Body: " + message.getBody(String.class));

			try {
				String url = JSONUtility.sliceJson(message.getBody(String.class), "/SubscribeURL", String.class);
				url = "https://icargo.ap-south-1.elasticbeanstalk.com/spapi/v1/trackings/platform/DHL/AV/awbs/134-39654381";
				log.debug("Url sliced from message:" + url);
				CloseableHttpClient closeableClient = HttpClients.createDefault();
				String authToken = "eyJjdHkiOiJhcHBsaWNhdGlvblwvanNvbiIsImFsZyI6IkhTMjU2In0.ewogICJjb3VudHJ5IiA6IG51bGwsCiAgIm93bl9haXJsaW5lX2NvZGUiIDogbnVsbCwKICAiZ3JhbnRlZEF1dGhvcml0eSIgOiAiU0NPUEVfREhMIiwKICAiYWlycG9ydF9jb2RlIiA6IG51bGwsCiAgImxhc3RfbmFtZSIgOiBudWxsLAogICJjb21wYW55LWNvZGUiIDogbnVsbCwKICAic3RhdGlvbl9jb2RlIiA6IG51bGwsCiAgImxhbmd1YWdlIiA6IG51bGwsCiAgImRlZmF1bHRfd2FyZWhvdXNlX2NvZGUiIDogbnVsbCwKICAicHJvZmlsZV9pZGVudGl0eSIgOiAiR0VORVJJQ19VU0VSIiwKICAicm9sZV9ncm91cF9jb2RlIiA6IG51bGwsCiAgInVzZXJfaWQiIDogImRobGFkbWluIiwKICAiaWNhcmdvX3Rva2VuIiA6IG51bGwsCiAgIm93bl9haXJsaW5lX2lkZW50aWZpZXIiIDogMCwKICAiZGVmYXVsdF9vZmZpY2VfY29kZSIgOiBudWxsLAogICJleHAiIDogMTU5MjQyNDY4MywKICAiZmlyc3RfbmFtZSIgOiBudWxsLAogICJleHBpcnlfZGF0ZV90aW1lIiA6ICIyMDIwLTA2LTE3VDIwOjExOjIzLjk0MjI5OFoiCn0.H_JhfIWxDs31TmLQdAXfjOU_9m6ChN9wvBEZqjSPIas";

				HttpGet getHttp = new HttpGet(url);
				getHttp.addHeader("ICO-Authorization", authToken);
				try {
					HttpResponse response = closeableClient.execute(getHttp);
					log.debug("Response from SNS service: " + response.getStatusLine());

				} catch (IOException e) {
					throw new IOException("Exception occured in " + NAME + ". Exception message: " + e);
				}
			} catch (InvalidJsonPathException e) {
				throw new InvalidJsonPathException("Exception occured in " + NAME + ". Exception message: " + e);

			}
			message.setFault(true);
		} else if (header.toString().equalsIgnoreCase(SNS_NOTIFICATN_HEADER_VALUE)) {
			log.debug("Notification Message Body: " + message.getBody(String.class));
			String notificatioMessage = null;
			try {

				notificatioMessage = JSONUtility.sliceJson(message.getBody(String.class), "/Message", String.class);

			} catch (InvalidJsonPathException e) {
				throw new InvalidJsonPathException("Exception occured in " + NAME + ". Exception message: " + e);

			}
			exchange.getIn().setBody(notificatioMessage);

		}

	}
}
