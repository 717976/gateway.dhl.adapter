package com.bless.plugin.adapter.outbound;

import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.springframework.stereotype.Component;

@Component

public class CacheConfiguration {

	public static CacheManager cacheManager;

	public static CacheManager getCacheManager() {

		cacheManager = CacheManagerBuilder
				.newCacheManagerBuilder().withCache("tokenCache", CacheConfigurationBuilder
						.newCacheConfigurationBuilder(String.class, String.class, ResourcePoolsBuilder.heap(10)))
				.build();
		cacheManager.init();

		return cacheManager;
	}
	}
