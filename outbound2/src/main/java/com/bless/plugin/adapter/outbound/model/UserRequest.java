package com.bless.plugin.adapter.outbound.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRequest {
	String username;
	String password;
	
	@JsonCreator
	public UserRequest(@JsonProperty("username") String username, @JsonProperty("password") String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	
	
	

}
