package com.bless.plugin.adapter.outbound.payload.serializer;


import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.Headers;
import org.apache.camel.Message;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.util.CaseInsensitiveMap;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.bless.plugin.adapter.outbound.CacheConfiguration;
import com.bless.plugin.adapter.sdk.bean.Payload;
import com.bless.plugin.client.utility.JSONUtility;

import lombok.extern.slf4j.Slf4j;

@Component(CaptureMasterAWBPayloadSerializer.NAME)
@Slf4j
public class CaptureMasterAWBPayloadSerializer {
	public static final String NAME="serializer.payload.postmasterawb";
    @Handler
    public void format(@Body Message message, @Body Payload payload, @Headers CaseInsensitiveMap headers, Exchange exchange) {
      
    	CacheManager cm = CacheConfiguration.cacheManager;
		Cache<String, String> tokenCache =cm.getCache("tokenCache", String.class, String.class);
		
		String channel =JSONUtility.sliceJson(payload.getTransformed().get(0), "/additional_info/channel",String.class); 
		String airline =JSONUtility.sliceJson(payload.getTransformed().get(0), "/additional_info/airline",String.class); 
		String awb =JSONUtility.sliceJson(payload.getTransformed().get(0), "/additional_info/awb",String.class); 
		JSONObject transformed=new JSONObject(payload.getTransformed().get(0));
		JSONObject body= transformed.getJSONObject("body");
		
		
		
        exchange.getIn().setHeader(Exchange.HTTP_METHOD, HttpMethods.POST.name());
        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "application/json");
        exchange.getIn().setHeader("ICO-Authorization", tokenCache.get("token"));
        headers.put("awb", "001-30024783");
        headers.put("channel", channel);
        headers.put("airline", airline);
        message.setBody(body, String.class);
    }

    protected String format(Payload payload) {
        return JSONUtility.toJSON(payload);
    }
}
