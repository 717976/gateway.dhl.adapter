package com.bless.plugin.adapter.outbound;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import org.apache.camel.CamelContext;
import org.apache.camel.component.http4.HttpComponent;
import org.apache.camel.util.jsse.KeyManagersParameters;
import org.apache.camel.util.jsse.KeyStoreParameters;
import org.apache.camel.util.jsse.SSLContextParameters;
import org.apache.camel.util.jsse.TrustManagersParameters;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SSLCertConfiguration {
	
	public HttpComponent setPath(CamelContext camel) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, GeneralSecurityException, IOException  {
		KeyStoreParameters ksp = new KeyStoreParameters();
	
    	ksp.setResource("keystoreibs21.jks");
		ksp.setPassword("Ibs12345");
		
		KeyManagersParameters kmp = new KeyManagersParameters();
		kmp.setKeyStore(ksp);
		kmp.setKeyPassword("Ibs12345");
		
		
		TrustManagersParameters trustManagersParam = new TrustManagersParameters();
		trustManagersParam.setKeyStore(ksp);
		
		
		SSLContextParameters scparam = new SSLContextParameters();
		scparam.setKeyManagers(kmp);
		scparam.setTrustManagers(trustManagersParam);
		
		HttpComponent httpComponent = camel.getComponent("https4", HttpComponent.class);
		httpComponent.setSslContextParameters(scparam);
		camel.setSSLContextParameters(scparam);
		log.info("Existing SSL Configuration");
		return httpComponent;
	}
	
	
}
