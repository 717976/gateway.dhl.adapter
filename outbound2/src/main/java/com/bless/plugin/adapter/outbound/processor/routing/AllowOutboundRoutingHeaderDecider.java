package com.bless.plugin.adapter.outbound.processor.routing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.camel.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bless.plugin.adapter.sdk.bean.Payload;
import com.bless.plugin.adapter.sdk.endpoint.Destination;
import com.bless.plugin.adapter.sdk.endpoint.Endpoints;
import com.bless.plugin.adapter.sdk.processor.routing.RoutingHeaderDecider;
import com.bless.plugin.client.exception.InvalidJsonPathException;
import com.bless.plugin.client.utility.JSONUtility;

import lombok.extern.slf4j.Slf4j;

@Component(AllowOutboundRoutingHeaderDecider.NAME)
@Slf4j
public class AllowOutboundRoutingHeaderDecider extends RoutingHeaderDecider<Destination> {

	public static final String NAME = "router.outbound.ibsapp";

	public static Map<String, String> destinationMap = new HashMap<String, String>();
	
	@Autowired
	@Qualifier("endpoints.destination")
	private Endpoints<Destination> DESTINATION;

	public static Map<String, String> prepareDestinationMap() {
		destinationMap.put("FLTSTATUSUPD", "trackShipment");
		destinationMap.put("MASTERAWB", "postmasterawb");
		destinationMap.put("HOUSEAWB", "posthouseawb");
		return destinationMap;
	}

	@Override
	public String whichDestination(Message message, Map<String, Object> headers, Map<String, Object> camelProperties,
			Throwable throwable) {
		System.out.println("Payload " + message.getExchange());

		prepareDestinationMap();

		List<Destination> destination = null;
		
		 try {
		  
		 
		  Payload payload = (Payload) message.getBody(); 
		  String messageID =JSONUtility.sliceJson(payload.getTransformed().get(0), "/additional_info/MessageID",String.class); 
		 
		  log.info("messageID " + messageID); 
		  
		  String destinationName = destinationMap.get(messageID); 
		  destination = this.destinations.getEndpoints().stream().filter(des ->
		  des.name().equals(destinationName)).collect(Collectors.toList());
		  
		  log.info("Destination picked: "+destination.get(0).name());
		
		  } catch
		  (InvalidJsonPathException e) {
		  
		 throw new InvalidJsonPathException("Exception occured in "+NAME+". Error Message: "+e); }
		 

		 return destination.get(0).name();
		
	}

	
	

}
