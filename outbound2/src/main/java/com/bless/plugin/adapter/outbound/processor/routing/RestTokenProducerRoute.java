package com.bless.plugin.adapter.outbound.processor.routing;

import java.util.Iterator;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spi.AsEndpointUri;
import org.apache.camel.spring.SpringRouteBuilder;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bless.plugin.adapter.outbound.CacheConfiguration;
import com.bless.plugin.adapter.outbound.model.UserRequest;
import com.bless.plugin.adapter.sdk.endpoint.Destination;
import com.bless.plugin.adapter.sdk.endpoint.Endpoints;

@Component(RestTokenProducerRoute.NAME)
public class RestTokenProducerRoute extends SpringRouteBuilder {

	public static final String NAME = "rest.token.producer";

	@AsEndpointUri
	private String authURL;
	
	@Value("${dhl.username}")
	private String username;
	
	@Value("${dhl.password}")
	private String password;

	@Autowired
	@Qualifier("endpoints.destination")
	private Endpoints<Destination> DESTINATION;

	@Override
	public void configure() throws Exception {
		CacheManager cm = CacheConfiguration.getCacheManager();
		Cache<String, String> tokenCache = cm.getCache("tokenCache", String.class, String.class);

		
		UserRequest request = new UserRequest(username, password);
		JSONObject json = new JSONObject(request);
	
		Iterator<Destination> itr = DESTINATION.getEndpoints().iterator();

		while (itr.hasNext()) {

			Destination dest = itr.next();
			if (dest.name().equalsIgnoreCase("authenticate")) {
				authURL = dest.uri();
				
			}
		}

		from("timer://test?period=240000")
		.log("after timer")
		.process(exchange -> {
			exchange.getIn().setBody(json.toString());
		})
		.setHeader(Exchange.HTTP_METHOD, constant("POST"))
				.setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
				.to(authURL)
				.process(new Processor() {
					public void process(Exchange exchange) throws Exception {
						String response = exchange.getIn().getBody(String.class);
						JSONObject json = new JSONObject(response);
						String token = json.getJSONObject("body").getString("id_token");
						tokenCache.put("token", token);
						
					}
				}).log("${body}");

	}
}