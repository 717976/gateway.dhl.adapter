package com.bless.plugin.adapter.outbound;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

import com.bless.plugin.adapter.sdk.processor.security.PermitAllAuthenticator;
import com.bless.plugin.adapter.sdk.processor.security.RestRouteAuthenticator;
import com.bless.plugin.adapter.sdk.transformer.TransformerClient;

import lombok.extern.slf4j.Slf4j;
import okhttp3.logging.HttpLoggingInterceptor;

@SpringBootApplication
@ComponentScan(basePackages = { "com.bless.plugin.adapter.sdk", "com.bless.plugin.adapter.outbound" })
@EnableAutoConfiguration
@EnableConfigurationProperties
@PropertySource(value = { "application.yml" })
@Slf4j
public class Application {
	@Value("${bless.transformer.url}")
	private String TRANSFORMER_URL;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}

	@Bean
	public CamelContextConfiguration contextConfiguration() {
		return new CamelContextConfiguration() {
			@Override
			public void beforeApplicationStart(CamelContext context) {
				context.setUseMDCLogging(true);
			}

			@Override
			public void afterApplicationStart(CamelContext camelContext) {
			}
		};
	}

	@Bean({ "authenticator.inbound.secondary.rest", "authenticator.outbound.primary" })
	public RestRouteAuthenticator restRouteAuthenticator() {
		return new PermitAllAuthenticator();
	}

	@Bean
	public TransformerClient transformerClient() throws Exception {
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		TransformerClient.init(TRANSFORMER_URL, interceptor);
		TransformerClient transformerClient = null;
		log.info("Transformer Client Bean Created");
		try {
			transformerClient = TransformerClient.getInstance();
		} catch (Exception e) {
			throw new Exception("Transformer client not initialized : call init() method first");
		}
		return transformerClient;
	}

}
